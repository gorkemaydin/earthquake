//
//  RequestTableModel.swift
//  DTP
//
//  Created by Gorkem Aydin on 21.05.2021.
//  Copyright © 2021 GorkemAydin. All rights reserved.
//

import Foundation

class RequestModel {
    
    private let method : HTTPRequstType
    private let path : String
    private let enablePop : Bool
    private let additionalPath : String?
    private let parameters : [String:String]?
    private let requestObj : Data?
    private let requestDelegate: RequestDelegete?
    private var id: Int64 = 0
    public var complated : (Data?) -> () = { _ in}
    
    public init(id: Int64 = 0,
                method: HTTPRequstType,
                path: String,
                enablePop: Bool,
                additionalPath: String?,
                requestObject: Data?,
                parameters: [String:String]?,
                requestDelegate: RequestDelegete?)
    {
        self.method = method
        self.path = path
        self.enablePop = enablePop
        self.additionalPath = additionalPath
        self.parameters = parameters
        self.requestObj = requestObject
        self.id = id
        self.requestDelegate = requestDelegate
    }
}
extension RequestModel {
    public func getMethod() -> HTTPRequstType{
        return method
    }
    public func getPath() -> String {
        return path
    }
    public func getEnablePop() -> Bool {
        return enablePop
    }
    public func getAdditionalPath() -> String? {
        return additionalPath
    }
    public func getParameters() -> [String:String]? {
        return parameters
    }
    public func getRequestObj() -> Data? {
        return requestObj
    }
    public func getId() -> Int64 {
        return id
    }
    public func SetId(id: Int64){
        return self.id = id
    }
    public func onComplated(complated: @escaping (Data?) -> ()){
        self.complated = { data in
//            guard let Base = ServiceUtil.shared.toObject(ofType: ResponseBaseModal<AnyCodable>.self, jsonData: data) else { complated(data); return }
//            if Base.GetCode() != 200 {
//                self.requestDelegate?.fail(by: self.getPath())
//            }else {
//                self.requestDelegate?.end(by: self.getPath(), data: data)
//            }
            complated(data)
        }
    }
}
