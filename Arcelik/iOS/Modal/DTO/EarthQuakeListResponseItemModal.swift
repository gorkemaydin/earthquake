//
//  EarthQuakeListResponseItemModal.swift
//  Arcelik
//
//  Created by Gorkem Aydin on 4.08.2021.
//

import Foundation

class EartQuakeListResponseItemModal : Codable {
    
    private let timestamp : Date?
    private let latitude: Double
    private let longitude : Double
    private let depth : Double
    private let size : Double
    private let quality: Double
    private let humanReadableLocation: String?
    
}
//MARK: -GET
extension EartQuakeListResponseItemModal {
    public func getTimestamp() -> Date? {
         return self.timestamp
    }
    public func getLatitude() -> Double {
        return self.latitude
    }
    public func getLongitude() -> Double {
        return self.longitude
    }
    public func getDepth() -> Double {
        return self.depth
    }
    public func getSize() -> Double {
        return self.size
    }
    public func getQuality() -> Double {
        return self.quality
    }
    public func getHumanReadableLocation() -> String? {
        return self.humanReadableLocation
    }
}
