//
//  EarthQuakeListResponseModal.swift
//  Arcelik
//
//  Created by Gorkem Aydin on 4.08.2021.
//

import Foundation

class EarthQuakeListResponseModal : Codable {

    private let results : [EartQuakeListResponseItemModal]?
}

extension EarthQuakeListResponseModal {
    
    public func getResults() -> [EartQuakeListResponseItemModal]? {
        return self.results
    }
}
