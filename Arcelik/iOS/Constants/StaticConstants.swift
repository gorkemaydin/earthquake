//
//  StaticConstants.swift
//  Arcelik
//
//  Created by Gorkem Aydin on 4.08.2021.
//

import Foundation

class StaticConstants {}

extension StaticConstants {
   
    class ServiceConstants {
        static let Apiversion = 1
        static let Api = ""
        static let CertURL = ""
        static let BaseURL = "https://apis.is/earthquake/"
        static let ApiURL = BaseURL + Api
        static let TimeOutRequest = TimeInterval(60 * 2)
        static let TimeOutResource = TimeInterval(60 * 2)

    }
}
