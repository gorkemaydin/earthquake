//
//  EarthquakeListItemView.swift
//  Arcelik
//
//  Created by Gorkem Aydin on 4.08.2021.
//


import UIKit

class EarthquakeListItemCell : UITableViewCell {
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    public var item : EartQuakeListResponseItemModal?{
        didSet {
            
            title.text = item?.getHumanReadableLocation()
            longitude.attributedText = getBoldNormalAttributeText(normalPart: item?.getLongitude().description, boldPart: "Longitude: ")
            latitude.attributedText = getBoldNormalAttributeText(normalPart: item?.getLatitude().description, boldPart: "Latitude: ")
            size.attributedText = getBoldNormalAttributeText(normalPart: item?.getSize().description, boldPart: "Size: ")
        }
    }
    private func getBoldNormalAttributeText(normalPart: String? , boldPart: String) -> NSMutableAttributedString {
        
        let attributsBold = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: .bold)]
        let attributsNormal = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15, weight: .regular)]
        
        let attributedString = NSMutableAttributedString(string: boldPart, attributes:attributsBold)
        let appendStringPart = NSMutableAttributedString(string: normalPart ?? String.Empty, attributes:attributsNormal)
        attributedString.append(appendStringPart)
        return attributedString
    }
    private let cellbackgroundView : UIView = {
        let _view = UIView()
        _view.translatesAutoresizingMaskIntoConstraints = false
        return _view
    }()
    private let title : UILabel = {
        let _label = UILabel()
        _label.translatesAutoresizingMaskIntoConstraints = false
        _label.textColor = .black
        _label.font =  UIFont.boldSystemFont(ofSize: 16)
        return _label
    }()
    private let longitude : UILabel = {
        let _label = UILabel()
        _label.translatesAutoresizingMaskIntoConstraints = false
        _label.textColor = .lightGray
        _label.textColor = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
        return _label
    }()
    private let latitude : UILabel = {
        let _label = UILabel()
        _label.translatesAutoresizingMaskIntoConstraints = false
        _label.textColor = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
        return _label
    }()
    private let size : UILabel = {
        let _label = UILabel()
        _label.translatesAutoresizingMaskIntoConstraints = false
        _label.textColor = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
        _label.textAlignment = .right
        return _label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
//MARK: SetupUI
extension EarthquakeListItemCell {
    
    private func setupUI() {
        setupBackgroundView()
        setupTitle()
        setupLatitude()
        setupLongitude()
        setupSize()
    }
    private func setupBackgroundView() {
        self.addSubview(cellbackgroundView)
        
        cellbackgroundView.layer.cornerRadius = 8
        cellbackgroundView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 8).isActive = true
        cellbackgroundView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8).isActive = true
        cellbackgroundView.topAnchor.constraint(equalTo: self.topAnchor, constant: 1).isActive = true
        cellbackgroundView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -1).isActive = true
    }
    private func setupTitle() {
        cellbackgroundView.addSubview(title)
        
        title.leadingAnchor.constraint(equalTo: cellbackgroundView.leadingAnchor, constant: 16).isActive = true
        title.topAnchor.constraint(equalTo: cellbackgroundView.topAnchor, constant: 8).isActive = true
        title.trailingAnchor.constraint(equalTo: cellbackgroundView.trailingAnchor,constant: 0).isActive = true
        title.numberOfLines =  0
        title.setContentHuggingPriority(.fittingSizeLevel, for: .vertical)
    }
    private func setupLatitude() {
        cellbackgroundView.addSubview(latitude)
        
        latitude.leadingAnchor.constraint(equalTo: cellbackgroundView.leadingAnchor, constant: 16).isActive = true
        latitude.topAnchor.constraint(equalTo: self.title.bottomAnchor, constant: 16).isActive = true
        latitude.numberOfLines =  0
        latitude.trailingAnchor.constraint(equalTo: cellbackgroundView.centerXAnchor,constant: 0).isActive = true
    }
    private func setupLongitude() {
        cellbackgroundView.addSubview(longitude)
        
        longitude.leadingAnchor.constraint(equalTo: cellbackgroundView.leadingAnchor, constant: 16).isActive = true
        longitude.bottomAnchor.constraint(equalTo: cellbackgroundView.bottomAnchor, constant: -8).isActive = true
        longitude.topAnchor.constraint(equalTo: self.latitude.bottomAnchor, constant: 4).isActive = true
        longitude.numberOfLines = 0
        longitude.trailingAnchor.constraint(equalTo: cellbackgroundView.centerXAnchor,constant: 0).isActive = true
        
    }
    private func setupSize() {
        cellbackgroundView.addSubview(size)
        
        size.leadingAnchor.constraint(equalTo: cellbackgroundView.centerXAnchor, constant: 0).isActive = true
        size.bottomAnchor.constraint(equalTo: cellbackgroundView.bottomAnchor, constant: -8).isActive = true
        size.numberOfLines = 0
        size.trailingAnchor.constraint(equalTo: cellbackgroundView.trailingAnchor,constant: -8).isActive = true
    }
}

