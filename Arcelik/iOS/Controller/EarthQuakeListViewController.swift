//
//  ViewController.swift
//  Arcelik
//
//  Created by Gorkem Aydin on 4.08.2021.
//

import UIKit

class EarthQuakeListViewController: UIViewController {
    
    private let earthQuakes : EarthQuakeListResponseModal
    private let cellId = "EarhQuakeListCell"
    
    init(earthQuakes: EarthQuakeListResponseModal) {
        self.earthQuakes = earthQuakes
        super.init(nibName: nil, bundle: nil)
        self.view.backgroundColor = .white
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private lazy var earhQuakeListtableView : UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
}
//MARK: -TableView DataSource & Delegate
extension EarthQuakeListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.earthQuakes.getResults()?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! EarthquakeListItemCell
        cell.item = earthQuakes.getResults()?[indexPath.row]
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
//MARK: -SetupUI
extension EarthQuakeListViewController {
    private func setupUI() {
        setupTableView()
    }
    private func setupTableView() {
        self.view.addSubview(earhQuakeListtableView)

        earhQuakeListtableView.register(EarthquakeListItemCell.self, forCellReuseIdentifier: cellId)
        
        earhQuakeListtableView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor,constant: 0).isActive = true
        earhQuakeListtableView.topAnchor.constraint(equalTo: self.view.topAnchor, constant:  0).isActive = true
        earhQuakeListtableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor,constant: 0).isActive = true
        earhQuakeListtableView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        
        print(self.earthQuakes.getResults()?.count ?? 0)
    }
}
