//
//  HTTPRequest.swift
//  IncomingInvoices
//
//  Created by Görkem Aydın on 10.02.2020.
//  Copyright © 2020 GorkemAydin. All rights reserved.
//

import UIKit
import Alamofire
#if os(watchOS)
import WatchKit
#endif
public class HTTPRequest{
    
    static let shared = HTTPRequest()
    private var afManager : Alamofire.SessionManager?

    internal func Service<T: Codable> (method: HTTPRequstType,
                                       path:String,
                                       enablePop : Bool = true,
                                       additionalPath : String? = nil,
                                       parameters:[String:String]? = nil,
                                       requestObj: T? ,
                                       complated : @escaping (_ data : Data?) -> Void = { _ in})
    {
        var _url = GetBaseURL(with: path, and: additionalPath)
        
        if parameters != nil{
            var queryItems = [URLQueryItem]()
            for parameter in parameters!{
                let queryItem = URLQueryItem(name: parameter.key, value: parameter.value)
                queryItems.append(queryItem)
            }
            _url?.queryItems = queryItems
        }
        guard let url = _url?.url else { return }
        var request = URLRequest(url: url)
        
        
        request.httpMethod = method.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("tr-tr", forHTTPHeaderField: "Language")
        
        #if os(iOS)
        request.setValue("iOS", forHTTPHeaderField: "DeviceType")
        if let version = Bundle.main.releaseVersionNumber {
            request.setValue(version, forHTTPHeaderField: "AppVersion")
        }
        if Bundle.main.buildVersionNumber > 0 {
            request.setValue(String(Bundle.main.buildVersionNumber), forHTTPHeaderField: "AppVersionCode")
        }

        #elseif os(watchOS)
        let watchOSVersion = WKInterfaceDevice.current().WatchSystemVersion
        request.setValue(watchOSVersion, forHTTPHeaderField: "AppVersion")
        request.setValue("watchOS", forHTTPHeaderField: "DeviceType")
        
        let token = UserDefaults.standard.string(forKey: "Token") ?? String.Empty
        if !token.isEmpty {
            request.setValue(token, forHTTPHeaderField: "DeviceId")
        }
        #endif
        
        
        if !(method == .Get || method == .Delete ) {
            do {
                let jsonEncoder = JSONEncoder()
                request.httpBody = try jsonEncoder.encode(requestObj)
            } catch {
              
            }
        }
        DispatchQueue.background(completion:{
            [weak self] in
            self?.AFRequest(path: path,
                      request: request,
                      enablePop: enablePop)
            {
                data in
                complated(data)
            
            }
        })
    }
    private func AFRequest(path : String,
                           request : URLRequest,
                           enablePop : Bool,
                           complated : @escaping (_ data : Data?) -> Void = {_ in}) {
        
        afManager = AFManagerUtil.shared.setupAFManager(for: path)
        afManager?.request(request).responseJSON {
            response in
            
            switch response.result{
            case .failure(let error):
              
                if error._code == NSURLErrorTimedOut {
                    let message = Toast(text: "TimeOut".Localized)
                    message.show()
                }
                
            default:
                break
            }
            complated(response.data)
        }
    }
    private func GetBaseURL(with path: String, and additionalPath: String?) -> URLComponents? {
        
        var url = URLComponents(string:  StaticConstants.ServiceConstants.ApiURL + path)
        
        
        if let addPath = additionalPath {
            url = URLComponents(string: StaticConstants.ServiceConstants.ApiURL + path + "/"  + addPath)
        }
        return url
    }
}
