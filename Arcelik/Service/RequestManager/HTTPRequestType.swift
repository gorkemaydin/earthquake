//
//  HTTPRequestType.swift
//  IncomingInvoices
//
//  Created by Görkem Aydın on 11.02.2020.
//  Copyright © 2020 GorkemAydin. All rights reserved.
//

import Foundation

enum HTTPRequstType : String {
   
    case Get = "GET"
    case Post = "POST"
    case Put = "PUT"
    case Delete = "DELETE"
    
}
