//
//  ServiceUtil.swift
//  IncomingInvoices
//
//  Created by Görkem Aydın on 11.02.2020.
//  Copyright © 2020 GorkemAydin. All rights reserved.
//

import Foundation

class ServiceUtil {
    
    static let shared = ServiceUtil()
   
    public let JSONNull = AnyCodable([:])
    private var decoder = JSONDecoder()
    
    public func toObject<T: Codable>(ofType : T.Type, jsonData : Data?) -> T? {
        guard let Data = jsonData else { return nil }
        decoder.dateDecodingStrategy = .formatted(DateFormatter.iso8601Full)
        
        do {
            return try decoder.decode(T.self, from: Data)
            
        }catch let error{
            print(error)
            return nil
        }
    }
}
//2017-10-13T09:37:21.000Z
extension DateFormatter {
  static let iso8601Full: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
    formatter.calendar = Calendar(identifier: .iso8601)
    formatter.timeZone = TimeZone(secondsFromGMT: 0)
    formatter.locale = Locale(identifier: "en_US_POSIX")
    return formatter
  }()
}
