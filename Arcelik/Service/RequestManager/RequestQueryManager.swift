//
//  RequestQueryManager.swift
//  Arcelik
//
//  Created by Gorkem Aydin on 4.08.2021.
//

import Foundation

class RequestQueryManager  {
    
    private var requests : [RequestModel]
    private let httpRequest : HTTPRequest
    
    init(requests : [RequestModel]) {
        self.requests = requests
        self.httpRequest = HTTPRequest()
    }
    
    public func makeRequest() {
        guard let request = requests.first else { return }
        
        let dispatchGroup = DispatchGroup()
        let dispatchQueue = DispatchQueue(label: "httpRequest")
        let dispatchSemaphore = DispatchSemaphore(value: 0)
        dispatchQueue.async {
            dispatchGroup.enter()
            
            self.httpRequest.Service(method: request.getMethod(),
                                     path: request.getPath(),
                                     enablePop: request.getEnablePop(),
                                     additionalPath: request.getAdditionalPath(),
                                     parameters: request.getParameters(),
                                     requestObj: request.getRequestObj())
            { (data) in
                
                dispatchSemaphore.signal()
                dispatchGroup.leave()
                request.complated(data)
            }
            dispatchSemaphore.wait()
        }
        dispatchGroup.notify(queue: dispatchQueue) {
            self.requests.removeFirst()
            if  self.requests.first  != nil {
                self.makeRequest()
            }
        }
    }
}
