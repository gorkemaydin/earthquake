//
//  MasterConversionLayer.swift
//  Arcelik
//
//  Created by Gorkem Aydin on 4.08.2021.
//

import Foundation

class MasterConversionLayer {
    
    static let shared = MasterConversionLayer()
    private let objectMapper = ServiceUtil()
    
    public func EarthquakeConversion(data : Data?) -> EarthQuakeListResponseModal? {
        guard let results = objectMapper.toObject(ofType: EarthQuakeListResponseModal.self, jsonData: data) else { return nil }
        return results
    }
}
