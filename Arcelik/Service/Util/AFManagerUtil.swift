//
//  SSLUtil.swift
//  IncomingInvoices
//
//  Created by Gorkem Aydin on 12.12.2020.
//  Copyright © 2020 GorkemAydin. All rights reserved.
//

import Foundation
import Alamofire

class AFManagerUtil {
    
    static let shared : AFManagerUtil = AFManagerUtil()
    
    public func setupAFManager(for path: String) -> Alamofire.SessionManager{
        let configuration = URLSessionConfiguration.default
        configuration.httpMaximumConnectionsPerHost = 10
        
        let afManager = Alamofire.SessionManager()
        
        afManager.session.configuration.timeoutIntervalForRequest = StaticConstants.ServiceConstants.TimeOutRequest
        afManager.session.configuration.timeoutIntervalForResource = StaticConstants.ServiceConstants.TimeOutResource
        
        return afManager
    }
}
