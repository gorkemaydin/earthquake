//
//  MasterLayer.swift
//  Arcelik
//
//  Created by Gorkem Aydin on 4.08.2021.
//

import Foundation

class MasterRequest {
    
    static let shared = MasterRequest()
    
    public func getEarthQuakeRequest() -> RequestModel{
        
        let request = RequestModel(method: .Get,
                                   path: ServicePaths.Master.EarthquakeList,
                                   enablePop: false,
                                   additionalPath: nil,
                                   requestObject: nil,
                                   parameters: nil,
                                   requestDelegate: nil)
        return request
    }
}
