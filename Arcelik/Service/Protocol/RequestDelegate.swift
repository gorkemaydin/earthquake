//
//  RequestDelagete.swift
//  Arcelik
//
//  Created by Gorkem Aydin on 4.08.2021.
//

import Foundation

protocol RequestDelegete {
    
    func start(by requestId : String)
    func end(by requestId : String, data : Data?)
    func fail(by requestId : String)
}
