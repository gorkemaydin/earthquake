//
//  StringHelper.swift
//  IncomingInvoices
//
//  Created by Görkem Aydın on 10.02.2020.
//  Copyright © 2020 GorkemAydin. All rights reserved.
//


import UIKit

extension String {
    
    static var Empty : String {
        return ""
    }
    static var CurrencyCode : String {
        return Locale.current.currencySymbol ?? "₺"
    }
    var IsNumber: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
    var Localized : String {
        return NSLocalizedString(self, comment: "")
    }
    
    func base64ToImage() -> UIImage {
          
          let dataDecoded : Data = Data(base64Encoded: self, options: .ignoreUnknownCharacters)!
          let decodedimage = UIImage(data: dataDecoded)
          return decodedimage!
      
    }
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
    func Base64StringToContentConverter(with name : String,contentType : String) -> URL? {

        guard
            var documentsURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last,
            let convertedData = Data(base64Encoded: self)
            else {
            return nil
        }
        documentsURL.appendPathComponent(name + contentType)

        do {
            try convertedData.write(to: documentsURL)
        } catch {
            //handle write error here
        }
        return documentsURL
    }
}
