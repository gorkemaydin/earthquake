//
//  BundleHelper.swift
//  IncomingInvoices
//
//  Created by Gorkem Aydin on 13.12.2020.
//  Copyright © 2020 GorkemAydin. All rights reserved.
//

import Foundation

extension Bundle {
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildVersionNumber: Int {
        return Int(infoDictionary?["CFBundleVersion"] as? String ?? String.Empty) ?? 0
    }
    var releaseVersionNumberPretty: String {
        return "v\(releaseVersionNumber ?? "1.0.0")"
    }
    var displayName: String? {
        return Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String
    }
}
